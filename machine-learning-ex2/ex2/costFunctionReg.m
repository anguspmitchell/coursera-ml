function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples
n = length(theta);

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

cumCost = 0;
cumGrad = zeros(size(theta));
for i = 1:m
    iCost = ((-1*y(i)*log(sigmoid(X(i,:)*theta))) - ((1 - y(i))*log(1-sigmoid(X(i,:)*theta))));
    cumCost = cumCost + iCost;
    
    cumGradTheta0 = cumGrad(1,1);
    cumGrad = cumGrad + ((sigmoid(X(i,:)*theta) - y(i)) .* X(i,:))' + (lambda .* theta ./ m);
    cumGrad(1, 1) = cumGradTheta0 + ((sigmoid(X(i,:)*theta) - y(i)) .* X(i,1));
end;

regSum = 0;
for j = 2:n
    regSum = regSum + theta(j) ^ 2;
end;

J = (cumCost / m) + (regSum * lambda / 2 / m);
grad = cumGrad ./ m;




% =============================================================

end
